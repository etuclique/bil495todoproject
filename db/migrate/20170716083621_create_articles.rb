class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.integer :issueId
      t.string :content
      t.date :dueDate
      t.date :settingDate
      t.boolean :isItDone
      t.boolean :overDue
      t.string :title

      t.timestamps null: false
    end
  end
end

Rails.application.routes.draw do
  get 'articles/index'

  get 'welcome/index'
 
  resources :articles
 
  root 'welcome#index'
  
  get 'articles/show'
end